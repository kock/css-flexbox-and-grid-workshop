'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass');


gulp.task('sass', function () {
    return gulp.src('scss/main.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(gulp.dest('css/'));
});


gulp.task('open', function () {
     gulp.watch('scss/**/*', { interval: 750 }, ['sass']);
});